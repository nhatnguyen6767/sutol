//
//  FourTabBarReusableView.swift
//  TEAM1
//
//  Created by net=0 on 12/10/20.
//

import UIKit

class HeaderView: UICollectionReusableView {
    
    static let identifier = "HeaderView"
    
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var fullnameLbl: UILabel!
    @IBOutlet weak var webTxt: UITextView!
    @IBOutlet weak var bioLbl: UILabel!
    @IBOutlet weak var posts: UILabel!
    @IBOutlet weak var followers: UILabel!
    @IBOutlet weak var followings: UILabel!
    @IBOutlet weak var postsTitle: UILabel!
    @IBOutlet weak var followersTitle: UILabel!
    @IBOutlet weak var followingsTitle: UILabel!
    @IBOutlet weak var editProfile_Btn: UIButton!
    
    
    static func nib() -> UINib {
        return UINib(nibName: "HeaderView", bundle: nil)
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    
    @IBAction func editProfile_Action(_ sender: Any) {
    }
    
}
