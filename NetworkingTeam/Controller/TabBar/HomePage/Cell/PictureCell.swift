//
//  FourTabBarCollectionViewCell.swift
//  TEAM1
//
//  Created by net=0 on 11/10/20.
//

import UIKit

class PictureCell: UICollectionViewCell {
    
    
    @IBOutlet weak var picImg: UIImageView!
    
    static let identifier = "PictureCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "PictureCell", bundle: nil)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

}
