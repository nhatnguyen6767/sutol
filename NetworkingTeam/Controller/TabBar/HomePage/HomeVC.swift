//
//  FourTabBarVC.swift
//  TEAM1
//
//  Created by net=0 on 11/10/20.
//

import UIKit
import Parse

class HomeVC: UIViewController {
    
    var refresher: UIRefreshControl!
    // how many pictures show, size of page
    var page: Int = 10
    
    var uuidArray = [String]()
    var picArray = [PFFileObject]()
    
    var collectionView: UICollectionView?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // set user name in the top
        self.navigationItem.title = PFUser.current()?.username?.uppercased()
        
        configCollectionView()
        
        refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        collectionView?.addSubview(refresher)
        
        // load post
        loadPosts()
        
    }
    
    @objc func refresh() {
        collectionView?.reloadData()
        refresher.endRefreshing()
    }
    
    func loadPosts() {
        
        let query = PFQuery(className: "posts")
        query.whereKey("username", contains: PFUser.current()!.username)
        query.limit = page
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                
                self.uuidArray.removeAll(keepingCapacity: false)
                self.picArray.removeAll(keepingCapacity: false)
                
                for object in objects! {
                    
                    // add found data to arrays
                    self.uuidArray.append(object.value(forKey: "uuid") as! String)
                    self.picArray.append(object.value(forKey: "pic") as! PFFileObject)
                }
                
                self.collectionView?.reloadData()
                
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView?.frame = view.bounds
    }
    
    func configCollectionView() {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        // space of header and cell or footer
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: view.frame.size.width / 2.2, height: view.frame.size.width / 2.2)
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        
        // register cell
        collectionView?.register(PictureCell.nib(), forCellWithReuseIdentifier: PictureCell.identifier)
        
        // register header
        collectionView?.register(HeaderView.nib(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HeaderView.identifier)
        
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.backgroundColor = .white
        view.addSubview(collectionView!)
        
        
    }


    

}

extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    // cell
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return picArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PictureCell.identifier, for: indexPath) as! PictureCell
        picArray[indexPath.row].getDataInBackground { (data, error) in
            if error == nil {
                cell.picImg.image = UIImage(data: data!)
            } else {
                print(error!.localizedDescription)
            }
        }
        return cell
    }
    
    
    // define size of each cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: view.frame.size.width / 4, height: view.frame.size.height / 4)
        return CGSize(width: 106, height: 106)
    }
    
    
    // header
    fileprivate func totalPosts(_ header: HeaderView) {
        // couting posts
        let posts = PFQuery(className: "posts")
        posts.whereKey("username", contains: PFUser.current()!.username)
        posts.countObjectsInBackground { (count, error) in
            if error == nil {
                header.posts.text = "\(count)"
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
    fileprivate func infoUser(_ header: HeaderView) {
        header.fullnameLbl.text = (PFUser.current()!.object(forKey: "fullname") as! String).uppercased()
        header.webTxt.text = PFUser.current()!.object(forKey: "web") as? String
        header.webTxt.sizeToFit()
        header.bioLbl.text = PFUser.current()?.object(forKey: "bio") as? String
        header.bioLbl.sizeToFit()
        
        let avaQuery = PFUser.current()?.object(forKey: "ava") as! PFFileObject
        avaQuery.getDataInBackground { (data, error) in
            header.imgAvatar.image = UIImage(data: data!)
        }
        header.editProfile_Btn.setTitle("edit profile", for: UIControl.State.normal)
    }
    
    fileprivate func totalFollowers(_ header: HeaderView) {
        // couting total followers
        let follower = PFQuery(className: "follow")
        follower.whereKey("following", contains: PFUser.current()!.username)
        follower.countObjectsInBackground { (count, error) in
            if error == nil {
                header.followers.text = "\(count)"
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
    fileprivate func totalFollowings(_ header: HeaderView) {
        // couting total following
        let follower = PFQuery(className: "follow")
        follower.whereKey("follower", contains: PFUser.current()!.username)
        follower.countObjectsInBackground { (count, error) in
            if error == nil {
                header.followings.text = "\(count)"
            } else {
                print(error!.localizedDescription)
            }
        }
    }
    
    fileprivate func tapAtPosts(_ header: HeaderView) {
        
        // tap posts
        let postsTap = UITapGestureRecognizer(target: self, action: #selector(postsTapping))
        postsTap.numberOfTapsRequired = 1
        header.posts.isUserInteractionEnabled = true
        header.posts.addGestureRecognizer(postsTap)
    }
    
    fileprivate func tapAtFollowers(_ header: HeaderView) {
        // follower tap
        let followersTap = UITapGestureRecognizer(target: self, action: #selector(followersTapping))
        followersTap.numberOfTapsRequired = 1
        header.followers.isUserInteractionEnabled = true
        header.followers.addGestureRecognizer(followersTap)
    }
    
    fileprivate func tapAtFollowings(_ header: HeaderView) {
        // following tap
        let followingsTap = UITapGestureRecognizer(target: self, action: #selector(followingsTapping))
        followingsTap.numberOfTapsRequired = 1
        header.followings.isUserInteractionEnabled = true
        header.followings.addGestureRecognizer(followingsTap)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HeaderView.identifier, for: indexPath) as! HeaderView
        
        infoUser(header)
        totalPosts(header)
        totalFollowers(header)
        totalFollowings(header)
        
        // Implement tap gestures (when tap to posts, follower, following)
        tapAtPosts(header)
        tapAtFollowers(header)
        tapAtFollowings(header)
        
        

        return header
    }
    
    @objc func followingsTapping() {
        
        user = PFUser.current()!.username!
        showing = "following"
        
        self.navigationController?.pushViewController(FollowersVC(), animated: true)
        
    }
    
    @objc func followersTapping() {
        user = PFUser.current()!.username!
        showing = "followers"
        
        self.navigationController?.pushViewController(FollowersVC(), animated: true)
    }
    
    @objc func postsTapping() {
        if !picArray.isEmpty {
            let index = NSIndexPath(item: 0, section: 0)
            self.collectionView?.scrollToItem(at: index as IndexPath, at: UICollectionView.ScrollPosition.top, animated: true)
        } else {
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.size.width, height: 200)
    }
    
    
}
