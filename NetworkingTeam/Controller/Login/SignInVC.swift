//
//  LoginVC.swift
//  NetworkingTeam
//
//  Created by net=0 on 9/10/20.
//

import UIKit
import FBSDKLoginKit
import Parse

class SignInVC: UIViewController {
    
    @IBOutlet weak var usernameTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var signBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var forgotBtn: UIButton!
    
    var fbLoginSuccess = false
    let activityIndicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // To set new font for name of product
//        usernameTxt.font = UIFont(name: "Pacifico", size: 25)
        
        self.navigationController?.isNavigationBarHidden = true
        hideKeyboardWhenTap()
        
        
        // after check login fb go to mainvc
//        checkUserIsValid()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
//        checkUserIsValid()
        
    }
    
    func checkUserIsValid(){
        
        // when open app, check token of user, if have go to mainvc
        if (AccessToken.current != nil && fbLoginSuccess == true) {
            FBManager.getFBUserData {
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.tabBarControllerView()
                
            }
        } else {
            
        }
        
    }
    
    func hideKeyboardWhenTap(){
        //Looks for single or multiple taps.
        let hideTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        hideTap.numberOfTapsRequired = 1
        self.view.isUserInteractionEnabled = true
        view.addGestureRecognizer(hideTap)
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func commonAlert(title: String, message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let ok = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func signInBtn_click(_ sender: Any) {
        dismissKeyboard()
        
        if usernameTxt.text!.isEmpty || passwordTxt.text!.isEmpty {
            commonAlert(title: "Please", message: "fill all fields")
        }
        // login
        PFUser.logInWithUsername(inBackground: usernameTxt.text!, password: passwordTxt.text!) { (user, error) in
            if error == nil {
                UserDefaults.standard.set(user!.username, forKey: "username")
                UserDefaults.standard.synchronize()
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.login()
                
            } else {
                let systemErr = error!.localizedDescription
                self.commonAlert(title: "ERROR!!!", message: systemErr)
            }
        }
        
    }
    
    
    @IBAction func facebookBtn_click(_ sender: Any) {
        dismissKeyboard()
        
        if (AccessToken.current != nil) {
            
            
            self.fbLoginSuccess = true
            self.viewDidAppear(true)
            
            
        } else {
            FBManager.shared.logIn(permissions: ["public_profile", "email"], from: self) { (result, error) in
                
                if (error == nil) {
                    FBManager.getFBUserData {
                        
                        self.fbLoginSuccess = true
                        self.viewDidAppear(true)
                        
                    }
                    
                } else {
                    print(error!.localizedDescription)
                }
                
            }
        }
        
        
    }
    
    func saveFacebookAccount(){
        
    }
    
    
    
    
    @IBAction func buttonSignUp(_ sender: Any) {
        self.navigationController?.pushViewController(SignUpVC(), animated: true)
        
    }
    
    
    @IBAction func forgotPassword(_ sender: Any) {
        self.navigationController?.pushViewController(ForgotPasswordVC(), animated: true)
    }
    
    
}
