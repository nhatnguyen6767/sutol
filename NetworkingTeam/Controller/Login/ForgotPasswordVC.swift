//
//  ForgotPasswordVC.swift
//  TEAM1
//
//  Created by net=0 on 11/10/20.
//

import UIKit
import Parse

class ForgotPasswordVC: UIViewController {

    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var resetBtn: CornerItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Reset Password"

        hideKeyboardWhenTap()
        
    }
    
    func hideKeyboardWhenTap(){
        //Looks for single or multiple taps.
        let hideTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        
        hideTap.numberOfTapsRequired = 1
        self.view.isUserInteractionEnabled = true
        view.addGestureRecognizer(hideTap)
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func commonAlert(title: String, message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let ok = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func resetBtn_Action(_ sender: Any) {
        dismissKeyboard()
        
        let userEmail = emailTxt.text
        
        if userEmail!.isEmpty {
            commonAlert(title: "Email field", message: "is empty")
        }
        
        PFUser.requestPasswordResetForEmail(inBackground: userEmail!)
        commonAlert(title: "Success", message: "send message to " + userEmail!)
        self.navigationController?.popViewController(animated: true)
        
    }
    
}
