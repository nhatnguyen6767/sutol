//
//  AppDelegate.swift
//  NetworkingTeam
//
//  Created by net=0 on 9/10/20.
//

import UIKit
import Parse
import Bolts
import FBSDKCoreKit



@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    var firstTabNavigationController : UINavigationController!
    var secondTabNavigationControoller : UINavigationController!
    var thirdTabNavigationController : UINavigationController!
    var homeTabBarNav : UINavigationController!
    
    fileprivate func testToDoLogin() {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        if let window = self.window {
            let loginVC = SignInVC()
            let nav = UINavigationController(rootViewController: loginVC)
            window.rootViewController = nav
            window.makeKeyAndVisible()
            // Do any additional setup after loading the view.
            let size = nav.navigationBar.frame.size
            nav.navigationBar.setBackgroundImage(imageWithColor(color: UIColor.clear, size: size, alpha: 0), for: UIBarMetrics.default)
            
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        ApplicationDelegate.shared.application( application, didFinishLaunchingWithOptions: launchOptions )
        
        parseConfig()
        login()
        // ham chay de viet chuc nang cho man hinh login, sau khi viet xong xoa ham nay va bat ham login len
//                testToDoLogin()
        
        
        return true
    }
    
    func tabBarControllerView() {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = UIColor.black
        let tabBarController = MainVC()
        
        firstTabNavigationController = UINavigationController.init(rootViewController: FirstTabBarVC())
        secondTabNavigationControoller = UINavigationController.init(rootViewController: SecondTabBarVC())
        thirdTabNavigationController = UINavigationController.init(rootViewController: ThirdTabBarVC())
        homeTabBarNav = UINavigationController.init(rootViewController: HomeVC())
        
        
        tabBarController.viewControllers = [firstTabNavigationController, secondTabNavigationControoller, thirdTabNavigationController,
            homeTabBarNav
        ]
        
        
        let item1 = UITabBarItem(title: "Not Home", image: nil, tag: 0)
        let item2 = UITabBarItem(title: "Contest", image: nil, tag: 1)
        let item3 = UITabBarItem(title: "Post a Picture", image: nil, tag: 2)
        let item4 = UITabBarItem(title: "Home", image: nil, tag: 3)
        
        
        firstTabNavigationController.tabBarItem = item1
        secondTabNavigationControoller.tabBarItem = item2
        thirdTabNavigationController.tabBarItem = item3
        homeTabBarNav.tabBarItem = item4
        
        
        // define default screen show first
        tabBarController.selectedIndex = 2//required value
        
        UITabBar.appearance().tintColor = UIColor(red: 0/255.0, green: 146/255.0, blue: 248/255.0, alpha: 1.0)
        
        self.window?.rootViewController = tabBarController
        
        window?.makeKeyAndVisible()
        
    }
    
    func parseConfig(){
        
        let parseConfig = ParseClientConfiguration {
            $0.applicationId = "id-team1-social-network"
            $0.clientKey = "master-key-team1-social-network"
            $0.server = "http://team1-social-network.herokuapp.com/parse"
        }
        Parse.initialize(with: parseConfig)
        
        
    }
    
    func changeRootViewController(_ vc: UIViewController, animated: Bool = true) {
        guard let window = self.window else {return}
        
        // change the root view controller to your specific view controller
        window.rootViewController = vc
        window.makeKeyAndVisible()
    }
    
    fileprivate func loginView() {
        if let window = self.window {
            let signInVC = SignInVC()
            let nav = UINavigationController(rootViewController: signInVC)
            window.rootViewController = nav
            window.makeKeyAndVisible()
            // Do any additional setup after loading the view.
            let size = nav.navigationBar.frame.size
            nav.navigationBar.setBackgroundImage(imageWithColor(color: UIColor.clear, size: size, alpha: 0), for: UIBarMetrics.default)
            
        }
    }
    
    func login() {
        // remember user's login
        let username:String? = UserDefaults.standard.string(forKey: "username")
        self.window = UIWindow(frame: UIScreen.main.bounds)
        // if logged in
        if username != nil {
            // check user if have username go to main screen
            tabBarControllerView()
  
        } else {
            // if don't have username go to login
            loginView()
            
        }
    }
    
    func application( _ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:] ) -> Bool { ApplicationDelegate.shared.application( app, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation] )
        
    }
    
    func imageWithColor(color: UIColor, size: CGSize, alpha: CGFloat) -> UIImage {
        UIGraphicsBeginImageContext(size)
        let currentContext = UIGraphicsGetCurrentContext()
        let fillRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        currentContext!.setFillColor(color.cgColor)
        currentContext!.setAlpha(alpha)
        currentContext!.fill(fillRect)
        let retval: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return retval
    }
    
    // MARK: UISceneSession Lifecycle
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    
}

